<?php

    
    if (isset($_POST['submit'])){
        echo "Formulario enviado correctamente" . "<br/>";
        
        $nombre=$_POST['nombre'];
        $telefono=$_POST['telefono'];
        $email=$_POST['email'];
        $ciudad=$_POST['ciudad'];
        $direccion=$_POST['direccion'];
        $masa=$_POST['masa'];
       // $extras=$_POST['extras'];
        
        if(!empty($nombre)){
            $nombre=filter_var($nombre, FILTER_SANITIZE_STRING);
            echo "Tu nombre: " . $nombre . "<br/>";
        } else {
            echo "Por favor, escriba su nombre" . "<br/>";
        }
        
        if(!empty($telefono)){
            $telefono=filter_var($telefono, FILTER_SANITIZE_STRING);
            echo "Teléfono: " . $telefono. "<br/>";
        } else {
            echo "Por favor, escriba su teléfono" . "<br/>";
        }
        
        if(!empty($email)){
            $email=filter_var($email, FILTER_SANITIZE_EMAIL);
            
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                echo "SPor favor, escribe tu EMAIL". "<br/>";
            } else {
                echo "Escribe tu email: " . $email . "<br/>"; 
        }
    }
        if(!empty($ciudad)){
            $ciudad=filter_var($ciudad, FILTER_SANITIZE_STRING);
            echo "Ciudad: " . $ciudad. "<br/>";
        } else {
            echo "Por favor, escriba su ciudad" . "<br/>";
        }

        if(!empty($direccion)){
            $direccion=filter_var($direccion, FILTER_SANITIZE_STRING);
            echo "Dirección: " . $direccion . "<br/>";
        } else {
            echo "Por favor, escriba su dirección" . "<br/>";
        }

        if(isset($_POST['size'])){
            $size = $_POST['size'];
        echo "Tamaño escogido: " . $size . "<br/>";
        } else {
        echo "Por favor, elija un tamaño" . "<br/>"; 
        }

        if(empty($masa)){
            echo "Por favor, seleccione una masa" . "<br/>"; 
        } else {
            echo "Masa escogida: " . $masa . "<br/>";
        }
        
        if(isset($_POST["extras"]))
        foreach($_POST["extras"] as $extra){
            echo "Extras marcados: " . $extra . "<br/>";
        }else{
            echo "Ningún extra escogido" . "<br/>";
        }
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>Pizzeria Toni Pepperoni</title>
    <meta charset="UTF-8">
    <meta name="UF1844" content="Formulario-evaluable UF1844">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="estilos.css" rel="stylesheet">
</head>
<body>
    
<header>
    <h1 class="tituloPrincipal">Pizzería Toni Pepperoni</h1>
</header>
    
    <h3 class="tituloSecundario">Formulario de pedidos</h3>
    
    <form class="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">

        <section class="datos">
            <label for="nombre">*Nombre:</label>
            <input type="text" name="nombre" id="nombre" style="color: white; font-size: 16px; font-weight: bold; height: 25px;text-shadow: black 0.1em 0.1em 0.2em;">

            <br>

            <label for="telefono">*Teléfono:</label>
            <input type="text" name="telefono" id="telefono" style="color: white; font-size: 16px; font-weight: bold; height: 25px; text-shadow: black 0.1em 0.1em 0.2em;">

            <br>

            <label for="email">*Email:</label>
            <input type="text" name="email" id="email" style="color: white; font-size: 16px;font-weight: bold; height: 25px;text-shadow: black 0.1em 0.1em 0.2em;">

            <br>

            <label for="ciudad">*Ciudad</label>
            <input type="text" name="ciudad" id="ciudad" style="color: white; font-size: 16px; font-weight: bold; height: 25px; text-shadow: black 0.1em 0.1em 0.2em;">

            <br>

            <label for="direccion">*Dirección:</label>
            <input type="text" name="direccion" id="direccion" style="color: white; font-size: 16px; font-weight: bold; height: 25px; text-shadow: black 0.1em 0.1em 0.2em;">

        </section>

        <section class="size">
            <h3>*Tamaño:</h3>

            <input type="radio" name="size" value="Pequeña" id="size">
            <label for="pequeña">Pequeña</label>

            <input type="radio" name="size" value="Mediana" id="size">
            <label for="mediana">Mediana</label>

            <input type="radio" name="size" value="Grande" id="size">
            <label for="grande">Grande</label>

        </section>
    
<!--MASA-->
        <section class="masa">
            <h3>*Masa:</h3>
            <select id="masa" name="masa">
                <option value="">Escoge una opción</option>
                <option value="fina">Fina</option>
                <option value="crujiente">Crujiente</option>
                <option value="gruesa">Gruesa</option>
                <option value="rellena">Rellena</option>
            </select>
        </section>    

        <!--EXTRAS-->    
            
        <section class="extras">
            <h3>Extras:</h3>
            
            <input type="checkbox" id="extras[]" name="extras[]" value="champiñones">
            <label for="champiñones">Champiñones</label> 
            <input type="checkbox" id="extras[]" name="extras[]" value="olivas">
            <label for="olivas">Olivas</label>
            <input type="checkbox" id="extras[]" name="extras[]" value="doblequeso">
            <label for="doblequeso">Doble Queso</label>
        </section>

<!--COMENTARIOS-->
        <section class="comentarios">
            <h3 class="comentariosh3">Comentarios sobre el pedido:</h3>
            <label for="w3review"></label>
            <textarea id="w3review" name="w3review" rows="10" cols="66" style="color: white; 
            font-size: 16px; font-weight: bold;  text-shadow: black 0.1em 0.1em 0.2em;"></textarea>
            <p class="obligatorio">*Todos los campos son obligatorios</p>

            <br>

            <input type="submit" name="submit" value="Enviar" class="boton">
        </section>
    </form>

<footer>
    
    <p>Fotografia de Narda Yescas (pexels.com)</p>

</footer>
</body>
</html>